// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_login_form/main.dart';
import 'package:flutter_login_form/src/login/login.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Login page tests', () {
    testWidgets('Main screen has One Login button',
        (WidgetTester tester) async {
      await tester.pumpWidget(RockersApp());

      var loginButtonTest = find.widgetWithText(ListTile, 'Login');
      expect(loginButtonTest, findsOneWidget);
    });

    testWidgets('Login page has Two input Field', (WidgetTester tester) async {
      await tester.pumpWidget(buildTestableWidget(LoginPage()));

      var inputFields = find.byType(FormInputField);
      expect(inputFields, findsNWidgets(2));

      final emailField = find.text('Email Address');
      expect(emailField, findsOneWidget);

      final passwordField = find.text('Password');
      expect(passwordField, findsOneWidget);
    });

    testWidgets('Login up page has three social media button',
        (WidgetTester tester) async {
      await tester.pumpWidget(buildTestableWidget(LoginPage()));

      var socialMediaLogin = find.byType(MediaButton);
      expect(socialMediaLogin, findsNWidgets(3));
    });
  });

  group('Sign up page tests', () {
    testWidgets('Sign up page has One Registration button',
        (WidgetTester tester) async {
      await tester.pumpWidget(buildTestableWidget(SignUpPage()));

      var registerButton = find.widgetWithText(ListTile, 'Register');
      expect(registerButton, findsOneWidget);
    });

    testWidgets('Sign up page has Two input Field',
        (WidgetTester tester) async {
      await tester.pumpWidget(buildTestableWidget(SignUpPage()));

      var inputFields = find.byType(FormInputField);
      expect(inputFields, findsNWidgets(3));

      final fullNameField = find.text('Full Name');
      expect(fullNameField, findsOneWidget);

      final emailField = find.text('Email Address');
      expect(emailField, findsOneWidget);

      final passwordField = find.text('Password');
      expect(passwordField, findsOneWidget);
    });

    testWidgets('Sign up page has three social media button',
        (WidgetTester tester) async {
      await tester.pumpWidget(buildTestableWidget(SignUpPage()));

      var socialMediaLogin = find.byType(MediaButton);
      expect(socialMediaLogin, findsNWidgets(3));
    });
  });

  group('Social Media button tests', () {
    testWidgets('Social media button has nothing', (WidgetTester tester) async {
      await tester.pumpWidget(buildTestableWidget(SocialMediaButtons()));

      var socialMediaLogin = find.byType(MediaButton);
      expect(socialMediaLogin, findsNothing);
    });

    testWidgets('Social media button has blank container',
        (WidgetTester tester) async {
      await tester.pumpWidget(buildTestableWidget(SocialMediaButtons()));

      var socialMediaLogin = find.byType(Container);
      expect(socialMediaLogin, findsOneWidget);
    });

    testWidgets('Social media button has one button',
        (WidgetTester tester) async {
      await tester.pumpWidget(buildTestableWidget(Card(
        child: SocialMediaButtons(
          allowedSignUps: [SocialMedia.Google],
        ),
      )));

      var socialMediaLogin = find.byType(MediaButton);
      expect(socialMediaLogin, findsOneWidget);
    });

    testWidgets('Social media button has three button',
        (WidgetTester tester) async {
      await tester.pumpWidget(buildTestableWidget(Card(
        child: SocialMediaButtons(
          allowedSignUps: [
            SocialMedia.Google,
            SocialMedia.FaceBook,
            SocialMedia.LinkedIn,
          ],
        ),
      )));

      var socialMediaLogin = find.byType(MediaButton);
      expect(socialMediaLogin, findsNWidgets(3));
    });
  });
}

Widget buildTestableWidget(Widget widget) {
  return MediaQuery(data: MediaQueryData(), child: MaterialApp(home: widget));
}
