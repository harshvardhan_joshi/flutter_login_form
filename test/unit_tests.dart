import 'package:flutter_login_form/src/login/login.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  // validator tests:
  group('All validator tests', () {
    //Full name validator
    group('Full Name validator', () {
      test('Null or empty not allowed', () {
        var fullName = '';
        final error = fullNameValidator(fullName);
        expect(error != null, true);
      });

      test('Only First name allowed', () {
        var fullName = 'John';
        final error = fullNameValidator(fullName);
        expect(error == null, true);
      });

      test('Only Last name allowed', () {
        var fullName = 'Doe';
        final error = fullNameValidator(fullName);
        expect(error == null, true);
      });

      test('Name initials allowed', () {
        var fullName = 'J D';
        final error = fullNameValidator(fullName);
        expect(error == null, true);
      });
    });

    // Password tests:
    group('Password validator', () {
      test('Null or empty not allowed', () {
        var password = '';
        final error = passwordValidator(password);
        expect(error != null, true);
      });

      test('Password smaller than 6 characters are not allowed', () {
        var password = 'John';
        final error = passwordValidator(password);
        expect(error != null, true);
      });

      test('Password with One character is not allowed', () {
        var password = 'P';
        final error = passwordValidator(password);
        expect(error != null, true);
      });

      test('Password with more than 5 characters are allowed', () {
        var password = 'PassWord';
        final error = passwordValidator(password);
        expect(error == null, true);
      });

      test('Custom Password test#1', () {
        var password = 'Rockers@123';
        final error = passwordValidator(password);
        expect(error == null, true);
      });

      test('Custom Password test#2', () {
        var password = 'R0ck3rs@123';
        final error = passwordValidator(password);
        expect(error == null, true);
      });
    });

    // Password tests:
    group('Email validator', () {
      test('Null or empty not allowed', () {
        var email = '';
        final error = emailValidator(email);
        expect(error != null, true);
      });

      test('Custom Email Test #1', () {
        var email = 'John';
        final error = emailValidator(email);
        expect(error != null, true);
      });

      test('Custom Email Test #2', () {
        var email = 'P';
        final error = emailValidator(email);
        expect(error != null, true);
      });

      test('Custom Email Test #3', () {
        var email = 'john@doe.com';
        final error = emailValidator(email);
        expect(error == null, true);
      });

      test('Custom Email Test #4', () {
        var email = 'rockers@123.com';
        final error = emailValidator(email);
        expect(error == null, true);
      });

      test('Custom Email Test #5', () {
        var email = 'john.doe@example.com';
        final error = emailValidator(email);
        expect(error == null, true);
      });

      test('Custom Email Test #6', () {
        var email = 'john.doe@127.0.0.1';
        final error = emailValidator(email);
        expect(error != null, true);
      });
    });
  });
}
