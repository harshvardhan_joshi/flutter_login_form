// import 'package:flutter/material.dart';
// import 'package:connectivity_plus/connectivity_plus.dart';
//
// /// --------- Logic ----------- ///
// class ConnectivityManager {
//   static Connectivity _connectivity = Connectivity();
//
//   static Stream<ConnectivityResult> get onConnectivityChange {
//     return _connectivity.onConnectivityChanged;
//   }
//
//   static Future<ConnectivityResult> get checkConnectivity {
//     return _connectivity.checkConnectivity();
//   }
// }
//
// /// --------- Widgets ----------- ///
// class ConnectivityChangeBuilder extends StatelessWidget {
//   final WidgetBuilder? connectedBuilder;
//   final WidgetBuilder? disconnectedBuilder;
//   final WidgetBuilder? connectivityLoadingBuilder;
//
//   const ConnectivityChangeBuilder({
//     Key? key,
//     @required this.connectedBuilder,
//     this.disconnectedBuilder,
//     this.connectivityLoadingBuilder,
//   })  : assert(connectedBuilder != null),
//         super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return StreamBuilder<ConnectivityResult>(
//       initialData: ConnectivityResult.wifi,
//       stream: ConnectivityManager.onConnectivityChange,
//       builder: (BuildContext context, AsyncSnapshot<ConnectivityResult> snapshot) {
//         var status;
//         if (snapshot.hasData) {
//           status = snapshot.data;
//         }
//         return ConnectivityBasedBuilder(
//           status: status,
//           connectedBuilder: connectedBuilder!,
//           disconnectedBuilder: disconnectedBuilder!,
//           loadingBuilder: connectivityLoadingBuilder!,
//         );
//       },
//     );
//   }
// }
//
// class ConnectivityBasedBuilder extends StatelessWidget {
//   final ConnectivityResult? status;
//   final WidgetBuilder? connectedBuilder;
//   final WidgetBuilder? disconnectedBuilder;
//   final WidgetBuilder? loadingBuilder;
//
//   const ConnectivityBasedBuilder({
//     Key? key,
//     @required this.connectedBuilder,
//     this.status,
//     this.disconnectedBuilder,
//     this.loadingBuilder,
//   })  : assert(connectedBuilder != null),
//         super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     if (status != null) {
//       switch (status) {
//         case ConnectivityResult.wifi:
//         case ConnectivityResult.mobile:
//           return connectedBuilder!(context);
//         case ConnectivityResult.none:
//           if (disconnectedBuilder != null) {
//             return disconnectedBuilder!(context);
//           } else {
//             return NoInternetWidget();
//           }
//         case ConnectivityResult.bluetooth:
//           // TODO: Handle this case.
//           break;
//         case ConnectivityResult.ethernet:
//           // TODO: Handle this case.
//           break;
//         default:
//           connectedBuilder!(context);
//           break;
//       }
//     }
//     if (loadingBuilder != null) {
//       return loadingBuilder!(context);
//     } else {
//       return LoadingConnectivityWidget();
//     }
//   }
// }

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ConnectivityManager {
  ConnectivityManager._() {
    _registerListener();
    // updateCurrentStatus();
  }

  static final ConnectivityManager _instance = ConnectivityManager._();

  factory ConnectivityManager() => _instance;

  final _connectivity = Connectivity();
  final ValueNotifier<bool?> onlineStateNotifier = ValueNotifier(null);

  Stream<ConnectivityResult> get connectivityStream => _connectivity.onConnectivityChanged.asBroadcastStream();

  bool get isOnline => onlineStateNotifier.value ?? false;

  bool get isOffline => !isOnline;

  void _registerListener() {
    connectivityStream.listen(_setNewStatus);
  }

  void _setNewStatus(ConnectivityResult event) {
    final previous = onlineStateNotifier.value;
    switch (event) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.ethernet:
      case ConnectivityResult.mobile:
        onlineStateNotifier.value = true;
        break;
      case ConnectivityResult.none:
      default:
        onlineStateNotifier.value = false;
        break;
    }

    if (previous == null) {
      return;
    }

    final message = 'App is ${isOnline ? 'Online' : 'Offline'}';
    // var context = NavigationService.context;
    // if (context != null) {
    //   if (isOnline) {
    //     showToastInfo(
    //       context: context,
    //       msg: message,
    //     );
    //   } else {
    //     NoInternetWidget();
    //   }
    // } else {
    //   showToast(message);
    // }
  }

  Future<void> updateCurrentStatus() async {
    final result = await _connectivity.checkConnectivity();
    _setNewStatus(result);
  }
}

final connectivityManager = ConnectivityManager();

class NoInternetWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Container(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.network_check_outlined,
                    color: Colors.red,
                    size: 48.0,
                  ),
                  SizedBox(height: 16.0),
                  Text(
                    'No Internet Connection',
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  SizedBox(height: 8.0),
                  Text(
                    'Either there is no connectivity or internet is slow.'
                    '\nMake sure Wi-Fi or Mobile data is On,'
                    ' Airplane mode is Off & try again.',
                    style: Theme.of(context).textTheme.subtitle1,
                    maxLines: 3,
                    softWrap: true,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class LoadingConnectivityWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Transform.scale(
                  scale: 0.75,
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(
                      Theme.of(context).accentColor,
                    ),
                  ),
                ),
                SizedBox(height: 16),
                Text(
                  'Please wait',
                  style: Theme.of(context).textTheme.headline6,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
