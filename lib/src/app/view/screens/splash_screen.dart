import 'package:flutter/material.dart';
import 'package:flutter_login_form/src/login/login.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              splashLogoAsset,
              fit: BoxFit.fitWidth,
              height: 150,
              width: 150,
              scale: 0.05,
            ),
            SizedBox(
              height: 24,
            ),
            SizedBox(
              width: 50,
              child: LinearProgressIndicator(
                minHeight: 1.0,
                backgroundColor: Theme.of(context).cardColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
