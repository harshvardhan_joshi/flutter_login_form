import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_login_form/src/app/app.dart';
import 'package:flutter_login_form/src/src.dart';

/// Body of the example app
class BodyWidget extends StatefulWidget {
  const BodyWidget({
    Key? key,
  }) : super(key: key);

  @override
  _BodyWidgetState createState() => _BodyWidgetState();
}

class _BodyWidgetState extends State<BodyWidget> {
  @override
  void initState() {
    _initializeAppServices();
    super.initState();
  }

  void _initializeAppServices() {
    // TODO: Perform initialization process required for any service
  }

  @override
  Widget build(BuildContext context) {
    final apiService = FirebaseApiService((e) {
      EasyLoading.dismiss();
    });

    return Builder(builder: (context) {
      return LoginPage(
        apiService: apiService,
        onLogin: (apiService, requestData) async {
          // TODO: Use this method to perform login process, i.e. calling api, handling response, etc.
          _onLogin(context, apiService, requestData);
        },
        onSignUp: (apiService, requestData) async {
          // TODO: Use this method to perform registration process, i.e. calling api, handling response, etc.
          final status = _onSignUp(context, apiService, requestData);
          return status;
        },
        onSocialMediaLoginSuccess: (user) {
          // TODO: Use the given user instance in app, consider this call back as sign in completed
          return _onSocialSignIn(context, user);
        },
        onLoginError: (error, [stacktrace]) {
          // TODO: Handle Login process specific errors here
          EasyLoading.dismiss();
          final errorMessage = getFormattedErrorMessage(error);
          print('_BodyWidgetState: build: errorMessage: $errorMessage');
          showToast(errorMessage);
          return null;
        },
        onSignUpError: (error, [stacktrace]) {
          // TODO: Handle Sign up process specific errors here
          EasyLoading.dismiss();
          showToast(getFormattedErrorMessage(error));
          return null;
        },
      );
    });
  }

  /// This method is implemented only for demo purpose
  Future<void> _onSocialSignIn(BuildContext context, AppUser user) {
    EasyLoading.dismiss();
    if (user == null) {
      return Future<void>.value(null);
    }
    return showProfilePage(
      context: context,
      user: user,
      onSignOut: () => signOutFromAll(),
    );
  }

  /// This method is implemented only for demo purpose
  Future<bool> _onSignUp(BuildContext context, ApiService apiService, Map<String, dynamic> requestData) async {
    final AppUser user = await apiService.signUp(requestData).catchError((e) {
      showToast(getFormattedErrorMessage(e));
      return null;
    });
    EasyLoading.dismiss();
    return _handleFirebaseApiSuccess(context, user, apiService);
  }

  bool _handleFirebaseApiSuccess(BuildContext context, AppUser user, ApiService apiService) {
    if (user == null) {
      return false;
    }
    showToast('Registration is Successful, Please login.');
    return true;
  }

  /// This method is implemented only for demo purpose
  Future<void> _onLogin(BuildContext context, ApiService apiService, Map<String, dynamic> requestData) async {
    final user = await apiService.signIn(requestData).catchError((e) {
      showToast(getFormattedErrorMessage(e));
      return null;
    });
    EasyLoading.dismiss();
    if (user == null) {
      return null;
    }
    showProfilePage(
      context: context,
      user: user,
      onSignOut: () => apiService.signOut(),
    );
  }

  /// This is just a helper method to make code readable
  ///
  /// [AppUser] instance will used to get full name, email and profile picture
  /// for display
  ///
  /// [onSignOut] callback is used when user clicks the sign out button on top-right
  /// of the profile page.
  ///
  /// sign out process changes according to which method was used to sign in,
  /// for google or facebook login, the process is different than the
  Future<void> showProfilePage({
    BuildContext? context,
    AppUser? user,
    VoidCallback? onSignOut,
  }) async {
    var pageRoute = MaterialPageRoute(
      builder: (context) => ProfilePage(
        user: user,
        onSignOut: onSignOut,
      ),
    );
    return showPage<void>(context!, pageRoute);
  }
}
