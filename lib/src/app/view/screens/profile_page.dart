import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_login_form/src/src.dart';

/// This is the page which displays the user data
/// when login or sign up is successful
class ProfilePage extends StatefulWidget {
  final AppUser? user;
  final VoidCallback? onSignOut;

  const ProfilePage({
    Key? key,
    @required this.user,
    this.onSignOut,
  })  : assert(user != null, 'user cannot be null'),
        super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  void initState() {
    EasyLoading.dismiss();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        widget.onSignOut!.call();
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text(
              'Profile',
              style: Theme.of(context).textTheme.subtitle2!.copyWith(
                    fontSize: 24,
                  ),
            ),
          ),
          backgroundColor: Theme.of(context).cardColor,
          iconTheme: IconThemeData(
            color: Theme.of(context).accentColor,
          ),
          actions: [
            buildSignOutButton(context),
          ],
        ),
        drawer: Drawer(
          child: ListView(
            children: _drawerItems(context),
          ),
        ),
        body: SafeArea(
          child: Center(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // User's profile picture, if url is not found then it will show a default picture
                  ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: _buildProfilePic(),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  // User's full name, if not available then it will be `--`
                  buildName(context, Theme.of(context).textTheme.headline4!),
                  // User's email, if not available then it will be `--`
                  Text(
                    widget.user!.email ?? '--',
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildName(BuildContext context, TextStyle style) {
    return Text(
      widget.user!.name ?? '--',
      style: style,
    );
  }

  IconButton buildSignOutButton(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.exit_to_app),
      onPressed: () {
        _signOut(context, onSignOut: widget.onSignOut!);
      },
    );
  }

  Widget _buildProfilePic() {
    return Image.network(
      widget.user!.profilePictureUrl ?? newUserProfileImageUrl,
      fit: BoxFit.cover,
      height: 150,
      width: 150,
    );
  }

  List<Widget> _drawerItems(BuildContext context) {
    // var allItems = _options.keys.map<Widget>((title) {
    //   final function = _options[title];
    //   return ListTile(
    //     title: Text(
    //       title,
    //       style: Theme.of(context).textTheme.subtitle2,
    //     ),
    //     onTap: () => function!.call(context),
    //   );
    // }).toList();

    var allItems = _options.map<Widget>((item) {
      return ListTile(
        leading: IconTheme(
          data: IconThemeData(
            color: Theme.of(context).accentColor,
          ),
          child: item.leadingIcon,
        ),
        title: Text(
          item.title,
          style: Theme.of(context).textTheme.subtitle2,
        ),
        onTap: () => item.onTap.call(context),
      );
    }).toList();
    final profileCard = _buildProfileCard();
    allItems.insert(0, profileCard);
    allItems.insert(
        1,
        Divider(
          height: 4.0,
        ));
    return allItems;
  }

  Widget _buildProfileCard() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Stack(
        fit: StackFit.loose,
        children: [
          Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 12,
                  ),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: _buildProfilePic(),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Align(
                    child: buildName(
                      context,
                      Theme.of(context).textTheme.headline6!,
                    ),
                    alignment: Alignment.bottomCenter,
                  ),
                ],
              ),
            ),
          ),
          // Align(
          //   alignment: Alignment.topRight,
          //   child: buildSignOutButton(context),
          // ),
        ],
      ),
    );
  }

  List<_DrawerEntry> get _options => <_DrawerEntry>[
        _DrawerEntry('Home', Icon(Icons.home), (context) {
          Navigator.of(context).maybePop();
        }),
        _DrawerEntry('Log out', Icon(Icons.exit_to_app), (context) {
          runAfterBuild((_) {
            Navigator.of(context).maybePop(null);
          });
          _signOut(context, onSignOut: widget.onSignOut!);
        }),
      ];

  // Map<String, Function(BuildContext context)> get _options =>
  //     <String, Function(BuildContext context)>{
  //       'Home': (context) {
  //         Navigator.of(context).maybePop();
  //       },
  //       'Log out': (context) {
  //         runAfterBuild((_) {
  //           Navigator.of(context).maybePop(null);
  //         });
  //         _signOut(context, onSignOut: widget.onSignOut);
  //       },
  //     };

  void _signOut(BuildContext context, {VoidCallback? onSignOut}) {
    onSignOut!.call();
    Navigator.of(context).maybePop(null);
  }
}

class _DrawerEntry {
  final String title;
  final Widget leadingIcon;
  final Function(BuildContext context) onTap;

  _DrawerEntry(this.title, this.leadingIcon, this.onTap);
}
