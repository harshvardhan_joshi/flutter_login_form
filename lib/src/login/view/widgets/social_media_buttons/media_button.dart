import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';

import '../../../login.dart';

// TODO: Customize the look of the Social media button here

/// The social media button widget which is visible on the login and registration
/// pages.
///
/// Each individual social media button has it's own implementation
/// of the ApiService to allow sign-in.
class MediaButton extends StatelessWidget {
  /// The URL which is used for fetching & showing picture for this button from
  /// the internet
  final String? imageUrl;

  /// The URI which is used for showing picture for this button from the assets
  final String? assetUri;

  /// The callback used for performing the sign in for this social media associated
  /// with this button
  final APICallback<AppUser>? socialMediaSignIn;

  /// The callback used for performing process when the sign-in is successful
  final AuthCallback? onSignInComplete;

  /// The apiService used for performing the sign in process
  final ApiService? apiService;

  const MediaButton({
    Key? key,
    this.imageUrl,
    this.assetUri,
    this.onSignInComplete,
    this.socialMediaSignIn,
    this.apiService,
  })  : assert(apiService != null,
            'No api service provided, it is required for social media login to work'),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    assert(imageUrl != null || assetUri != null,
        'Can not load social media icon without asset uri or url');
    assert(socialMediaSignIn != null,
        'Could not start process without proper api callback for social media sign in/sign up');
    return InkWell(
      onTap: () {
        EasyLoading.show()
            .then((_) => socialMediaSignIn!(apiService!, {}))
            .then(onSignInComplete!)
            .whenComplete(() => EasyLoading.dismiss());
      },
      child: Container(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 12),
          child: Card(
            elevation: 0,
            child: Center(
              child: buildLogo(),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildLogo() {
    var image;
    if (imageUrl != null) {
      image = Image.network(
        imageUrl!,
      );
    } else if (assetUri != null) {
      image = SvgPicture.asset(assetUri!);
    }
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12),
      child: Container(
        child: image,
      ),
    );
  }
}
