import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import '../../../login.dart';
import 'media_button.dart';

/// This enum represents which social media buttons are supported in this widget
///
/// You can add other social media with their custom implementation by adding the
/// new social media in this enum list.
enum SocialMedia {
  Google,
  FaceBook,
  Twitter,
  LinkedIn,
}

/// The Widget which shows the social media icons
///
/// The flow of this widget ensures that the sign-in flow for any social media
/// is complete and easily accessible.
///
/// Steps to add new social media:
///   1. add new entry in [SocialMedia] enum
///   2. implement ApiService for the new social media
///   3. use that api to handle sign-in process here
class SocialMediaButtons extends StatelessWidget {
  /// Callback to execute when the sign-in process is complete
  final AuthCallback? onSignInComplete;

  /// Callback to execute when the sign-in process is interrupted by any errors
  /// or exception
  final AuthErrorCallback? onError;

  /// The list of social media to display in the page
  ///
  /// Note: this allows adding duplicate entries.
  final List<SocialMedia>? allowedSignUps;

  const SocialMediaButtons({
    Key? key,
    this.onSignInComplete,
    this.onError,
    this.allowedSignUps,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (isEmpty(allowedSignUps)) return Container();
    final buttons = _getButtons(context);
    var itemCount = buttons.length;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 16),
      child: Container(
        child: GridView.count(
          shrinkWrap: true,
          crossAxisCount: (itemCount > 3) ? 3 : itemCount,
          children: buttons,
          childAspectRatio: childAspectRatio,
          physics: NeverScrollableScrollPhysics(),
        ),
      ),
    );
  }

  /// Getter to aspect ratio value which we use to handle button sizes in the List
  /// based on number of social media buttons to be shown
  double get childAspectRatio {
    final count = allowedSignUps!.length;
    switch (count) {
      case 1:
        return 3.25;
      case 2:
        return 1.75;
      case 3:
        return 1.25;
    }
    return 1;
  }

  /// Method to get Social media button for provided social media enums
  List<Widget> _getButtons(BuildContext context) {
    return allowedSignUps!.map((socialMedia) => _getMediaButton(context, socialMedia)).toList();
  }

  /// Method to build a Social media button for given enum value
  Widget _getMediaButton(BuildContext context, SocialMedia _socialMedia) {
    String _socialMediaImageAsset;
    ApiService _apiService;

    switch (_socialMedia) {
      case SocialMedia.Google:
        _apiService = GoogleApiServiceImpl(_socialSignInError);
        _socialMediaImageAsset = googleImageAsset;
        break;
      case SocialMedia.FaceBook:
      // _apiService = FacebookApiServiceImpl(_socialSignInError);
      // _socialMediaImageAsset = facebookImageAsset;
      // break;
      case SocialMedia.Twitter:
        _apiService = TwitterApiServiceImpl(_socialSignInError);
        _socialMediaImageAsset = twitterImageAsset;
        break;
      case SocialMedia.LinkedIn:
        LinkedInApiServiceImpl.initializeLinkedInLogin(context);
        _apiService = LinkedInApiServiceImpl(
          _socialSignInError,
          showLoading: () => EasyLoading.show(),
          stopLoading: () => EasyLoading.dismiss(),
        );
        _socialMediaImageAsset = linkedInImageAsset;
        break;
    }

    return MediaButton(
      apiService: _apiService,
      assetUri: _socialMediaImageAsset,
      socialMediaSignIn: _socialSignIn,
      onSignInComplete: onSignInComplete!,
    );
  }

  /// Method to trigger the sign in process for selected social media
  Future<AppUser> _socialSignIn(ApiService apiService, Map<String, dynamic> requestData) async {
    AppUser user = await apiService.signIn(requestData).catchError((e) {
      return _socialSignInError(e);
    });
    return user;
  }

  /// Method to catch any errors in sign-in process for any social media
  Future<dynamic> _socialSignInError(Object error, [stackTrace]) {
    EasyLoading.dismiss();
    onError!.call(error, stackTrace);
    return Future<dynamic>.value(null);
  }
}
