import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import '../../login.dart';

/// Registration/Sign up page
class SignUpPage extends StatefulWidget {
  /// The callback used for performing tasks when user clicks the login button
  /// on Login page, as both Login and Registration pages are linked, we
  /// can provide this callbacks that are used in other page as well.
  ///
  /// It can be used to perform API call for login process.
  /// It takes a Map of request data, which should be sent as parameters in login
  /// request to API, this map of data resembles a simple json data structure.
  ///
  /// Also the callback must Return a [AppUser] instance created with the data
  /// that is received from the API.
  ///
  /// Following can be done within this callback:
  ///   - Any network call to API for login
  ///   - Validating the response and Creating [AppUser] instance as final result
  ///   - Catching or Throwing exception in case of failure in login process
  ///
  /// The [AppUser] Model can be updated as per the requirement. Also, creating a factory method
  /// to get [AppUser] instance from any other type of data is recommended.
  ///
  /// For reference,
  /// Check [User.fromFirebaseUser(firebaseUser)] Method in [AppUser] class.
  ///
  /// Note: This callback is supposed to be used for custom API calls only.
  final APICallback<void>? onLogin;

  /// The callback used for performing tasks when custom login process is executed
  /// by the [onLogin] or any social media icons.
  ///
  /// This callback can be used to handle failure scenarios.
  /// Following can be done within this callback:
  ///   - Identify and Handle the Error
  ///   - Re-throw the error if required to handle it from the caller method/widget
  ///
  ///
  /// This method will provide an instance of [Error], with some description and
  /// stack trace of the cause of exception.
  ///
  /// It can return any Custom Error as long as the custom error extends the [Error]
  /// class.
  ///
  /// For reference,
  /// Check [AuthError] or [APIError] class for Customized Errors
  final AuthErrorCallback? onLoginError;

  /// The callback used for performing tasks when user clicks any social media
  /// icons for Sign up process. i.e Login with google, facebook etc.
  ///
  /// It does not take custom parameters, but it provides a [AppUser]
  /// parameter to perform any additional tasks that are required after the login
  /// process is completed successfully
  ///
  /// The [AppUser] Model can be updated as per the requirement.
  /// Also, creating a factory method to get [AppUser] instance from any other
  /// type of data is recommended.
  ///
  /// For reference,
  /// Check [User.fromFirebaseUser(firebaseUser)] Method in [AppUser] class.
  ///
  /// Note: This callback is supposed to be used for Social media Login only.
  /// Also, the same callback is used for all requested social media
  final AuthCallback? onSocialMediaLoginSuccess;

  /// The callback used for performing tasks when user clicks the Register button
  /// on registration page, as both Login and Registration pages are linked, we
  /// can provide this callbacks that are used in other page as well.
  ///
  /// It can be used to perform API call for registering process.
  /// It takes a Map of request data, which should be sent as parameters in Sign Up
  /// request to API, this map of data resembles a simple json data structure.
  ///
  /// Also the callback must Return a [AppUser] instance created with the data
  /// that is received from the API.
  ///
  /// Following can be done within this callback:
  ///   - Any network call to API for Sign Up
  ///   - Validating the response and Creating [AppUser] instance as final result
  ///   - Catching or Throwing exception in case of failure in Sign Up process
  ///
  /// The [AppUser] Model can be updated as per the requirement. Also, creating a factory method
  /// to get [AppUser] instance from any other type of data is recommended.
  ///
  /// For reference,
  /// Check [User.fromFirebaseUser(firebaseUser)] Method in [AppUser] class.
  ///
  /// Note: This callback is supposed to be used for custom API calls only.
  final APICallback<bool>? onSignUp;

  /// The callback used for performing tasks when custom Sign up process is executed
  /// by the [onSignUp] or any social media icons.
  ///
  /// This callback can be used to handle failure scenarios.
  /// Following can be done within this callback:
  ///   - Identify and Handle the Error
  ///   - Re-throw the error if required to handle it from the caller method/widget
  ///
  ///
  /// This method will provide an instance of [Error], with some description and
  /// stack trace of the cause of exception.
  ///
  /// It can return any Custom Error as long as the custom error extends the [Error]
  /// class.
  ///
  /// For reference,
  /// Check [AuthError] or [APIError] class for Customized Errors
  final AuthErrorCallback? onSignUpError;

  /// This api service will be used for sign up process
  final ApiService? apiService;

  const SignUpPage({
    Key? key,
    this.onLogin,
    this.onLoginError,
    this.onSocialMediaLoginSuccess,
    this.onSignUp,
    this.onSignUpError,
    this.apiService,
  }) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a `GlobalKey<FormState>` which should be unique to each Page
  // with a form.
  final _formKey = GlobalKey<FormState>();

  /// Value of full name provided by user will be stored here
  var fullName;

  /// Value of email provided by user will be stored here
  var email;

  /// Value of password provided by user will be stored here
  var password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: Theme.of(context).canvasColor,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Center(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      // Title of the screen
                      Text(
                        'Register',
                        style: Theme.of(context).textTheme.headline4,
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        'Register to continue',
                        style: Theme.of(context).textTheme.subtitle2!.copyWith(color: ThemeUtils.textColor.withOpacity(0.6)),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      // Social media sign up buttons
                      SocialMediaButtons(
                        allowedSignUps: [
                          SocialMedia.Google,
                          SocialMedia.FaceBook,
                          SocialMedia.LinkedIn,
                        ],
                        onSignInComplete: (user) async {
                          _resetPageState();
                          EasyLoading.dismiss();
                          if (user == null) return;
                          // TODO this call back will be called for all social media

                          /// Social media specific implementation is already done
                          /// Just use this call back to handle the final output
                          /// of the sign in process.
                          ///
                          /// The [user] instance provided by this method is of a Custom
                          /// model created for this demo called [AppUser]
                          ///
                          /// Feel free to update the [AppUser] model or replace it with any other
                          /// object if required.

                          EasyLoading.show();
                          widget.onSocialMediaLoginSuccess!.call(user);
                        },
                        onError: (error, [stackTrace]) async {
                          _resetPageState();
                          EasyLoading.dismiss();
                          widget.onSignUpError!.call(error, stackTrace);
                        },
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        'or Register with Email',
                        style: Theme.of(context).textTheme.subtitle2!.copyWith(color: ThemeUtils.textColor.withOpacity(0.6)),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      // Manual Sign up form
                      buildTextInputFields(),
                      SizedBox(
                        height: 12,
                      ),
                      // Register button
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 8),
                        child: buildRegisterButton(context),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24),
                        child: buildLoginNow(context),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Make changes here regarding the 'Login Now' UI or function, if required
  Widget buildLoginNow(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('Already have an account?'),
        SizedBox(width: 8),
        InkWell(
          onTap: () {
            _showLoginPage(context);
          },
          child: Text(
            'Login now',
            style: Theme.of(context).textTheme.subtitle1!.copyWith(
                  color: Theme.of(context).accentColor,
                  fontWeight: FontWeight.bold,
                ),
          ),
        ),
      ],
    );
  }

  void _showLoginPage(BuildContext context) {
    _resetPageState();
    showLoginPage(
      context: context,
      apiService: widget.apiService,
      onLogin: widget.onLogin,
      onLoginError: widget.onLoginError,
      onSignUp: widget.onSignUp,
      onSignUpError: widget.onSignUpError,
      onSocialMediaLoginSuccess: widget.onSocialMediaLoginSuccess,
    );
  }

  /// Make changes here regarding the Input Field required for this page, add or remove fields
  /// UI changes in the field are done in [FormInputField] class
  Widget buildTextInputFields() {
    // TODO: add remove input fields here for registration form
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FormInputField(
              title: 'Full Name',
              inputType: TextInputType.emailAddress,
              inputAction: TextInputAction.next,
              validator: fullNameValidator,
              onSaved: (value) {
                return fullName = value;
              },
            ),
            FormInputField(
              title: 'Email Address',
              inputType: TextInputType.emailAddress,
              inputAction: TextInputAction.next,
              validator: emailValidator,
              onSaved: (value) {
                return email = value;
              },
            ),
            FormInputField(
              title: 'Password',
              inputType: TextInputType.text,
              inputAction: TextInputAction.go,
              validator: passwordValidator,
              obscureText: true,
              onSaved: (value) {
                return password = value;
              },
            ),
          ],
        ),
      ),
    );
  }

  /// Make changes here regarding the 'Register' button UI or function, if required
  Widget buildRegisterButton(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Theme.of(context).accentColor,
      ),
      child: ListTile(
        title: Center(
          child: Text(
            'Register',
            style: Theme.of(context).textTheme.subtitle2!.copyWith(color: Colors.white, fontSize: 16.0),
          ),
        ),
        trailing: Icon(
          Icons.arrow_forward_outlined,
          color: Colors.white,
        ),
        onTap: () {
          _processSignUp(context);
        },
      ),
    );
  }

  /// This method should handle all intermediate process before the actual
  /// sign up callback is called.
  ///
  /// For example, fetching and processing input data & validation to be done
  /// before calling sign up callback.
  _processSignUp(BuildContext context) async {
    final isAllValidated = _formKey.currentState!.validate();
    if (isAllValidated) {
      /// save all the values if validation passes
      _formKey.currentState!.save();

      // TODO: replace data here to customize registration parameters sent to api
      final requestData = <String, dynamic>{
        KEY_FULL_NAME: fullName,
        KEY_EMAIL: email,
        KEY_PASSWORD: password,
      };

      final apiService = widget.apiService;

      assert(apiService != null, 'Api Service is required for manual login to work');

      EasyLoading.show();
      // call sign up method and process resulting data
      final success = await widget.onSignUp!.call(apiService!, requestData).catchError((e, s) {
        widget.onSignUpError!.call(e, s);
        return false;
      });

      // show login page if registration is Successful
      if (success) {
        _showLoginPage(context);
      }
    }
  }

  /// This method is used to dismiss all the validation messages from the
  /// text form fields in the form associated with [_formKey]
  void _resetPageState() {
    setState(() {
      _formKey.currentState!.reset();
    });
  }
}
