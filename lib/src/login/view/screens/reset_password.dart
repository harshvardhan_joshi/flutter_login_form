import 'package:flutter/material.dart';

import '../../login.dart';

/// Reset password page
class ResetPassword extends StatefulWidget {
  /// An API callback to execute when user presses the continue button
  ///
  /// This call should:
  ///  - contact the API responsible for Rest password module
  ///  - send new password & get a response from the API
  ///  - process and return the response
  final APICallback<Map<String, dynamic>>? resetPassword;

  /// api used for processing the reset password
  final ApiService? apiService;

  const ResetPassword({
    Key? key,
    @required this.resetPassword,
    this.apiService,
  }) : super(key: key);

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a `GlobalKey<FormState>` which should be unique to each Page
  // with a form.
  final _formKey = GlobalKey<FormState>();

  var _password;
  var _confirmPassword;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(
          color: ThemeUtils.textColor,
        ),
        leading: buildBackButton(),
      ),
      body: SafeArea(
        child: Container(
          color: Theme.of(context).canvasColor,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Center(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      // Title of the screen
                      Text(
                        'Reset Password',
                        style: Theme.of(context).textTheme.headline4,
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 36),
                        child: Text(
                          'Set new password for your account '
                          '\nso you can login and access all the features.',
                          style: Theme.of(context).textTheme.subtitle2!.copyWith(
                                fontSize: 14,
                                color: ThemeUtils.textColor.withOpacity(0.6),
                              ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      // Manual Login input fields
                      buildTextInputFields(),
                      SizedBox(
                        height: 16,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 8),
                        child: buildProceedButton(context),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Back button widget which is shown on top left of the screen
  Widget buildBackButton() {
    return IconButton(
      icon: Icon(Icons.chevron_left),
      onPressed: () => Navigator.of(context).maybePop(null),
    );
  }

  /// Make changes here regarding the Input Field required for this page, add or remove fields
  /// UI changes in the field are done in [FormInputField] class
  Widget buildTextInputFields() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FormInputField(
              title: 'New Password',
              inputType: TextInputType.visiblePassword,
              inputAction: TextInputAction.next,
              validator: passwordValidator,
              obscureText: true,
              onSaved: (value) => _password = value,
              onChange: (value) => _password = value,
            ),
            FormInputField(
              title: 'Confirm Password',
              inputType: TextInputType.visiblePassword,
              inputAction: TextInputAction.done,
              obscureText: true,
              validator: (value) {
                return confirmPasswordValidator(_password, _confirmPassword);
              },
              onSaved: (value) => _confirmPassword = value,
              onChange: (value) => _confirmPassword = value,
            ),
          ],
        ),
      ),
    );
  }

  /// Make changes here regarding the 'Login' button UI or function, if required
  Widget buildProceedButton(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Theme.of(context).accentColor,
      ),
      child: ListTile(
        title: Center(
          child: Text(
            'Continue',
            style: Theme.of(context).textTheme.subtitle2!.copyWith(color: Colors.white, fontSize: 16.0),
          ),
        ),
        trailing: Icon(
          Icons.arrow_forward_outlined,
          color: Colors.white,
        ),
        onTap: () {
          _proceed(context);
        },
      ),
    );
  }

  /// This method should handle all intermediate process before the actual
  /// login callback is called.
  ///
  /// For example, fetching and processing input data & validation to be done
  /// before calling login callback.
  void _proceed(BuildContext context) {
    final isAllValidated = _formKey.currentState!.validate();
    if (isAllValidated) {
      /// save all the values if validation passes
      _formKey.currentState!.save();

      // Gather Request data for sending a login request
      final requestData = <String, dynamic>{
        KEY_PASSWORD: _password,
      };
      // call login method and process resulting data
      widget.resetPassword!.call(widget.apiService!, requestData).then((response) {
        showToast('Reset password successful');

        // Return the user instance to Called Widget
        Navigator.of(context).maybePop(null);
      });
    }
  }
}
