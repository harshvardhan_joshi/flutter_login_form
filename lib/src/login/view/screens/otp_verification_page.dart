import 'package:flutter/material.dart';
import 'package:pinput/pin_put/pin_put.dart';

import '../../login.dart';

/// Code verification screen
class CodeVerificationPage extends StatefulWidget {
  /// This callback is used when user press continue button
  final APICallback<bool>? verifyOTP;

  /// api used for processing the verification of the code provided by the
  /// user in this page
  final ApiService? apiService;

  const CodeVerificationPage({
    Key? key,
    @required this.verifyOTP,
    this.apiService,
  }) : super(key: key);

  @override
  _CodeVerificationPageState createState() => _CodeVerificationPageState();
}

class _CodeVerificationPageState extends State<CodeVerificationPage> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a `GlobalKey<FormState>` which should be unique to each Page
  // with a form.
  final _formKey = GlobalKey<FormState>();

  /// used for gaining focus on the first input box
  FocusNode? _focusNode;

  /// OTP/Code value will be stored in this string
  String? _otp;

  TextEditingController? _controller;

  @override
  void initState() {
    _focusNode = FocusNode();
    _controller = TextEditingController();

    runAfterBuild((_) {
      _focusNode!.requestFocus();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(
          color: ThemeUtils.textColor,
        ),
        leading: buildBackButton(),
      ),
      body: SafeArea(
        child: Container(
          color: Theme.of(context).canvasColor,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Center(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      // Title of the screen
                      Text(
                        'Enter 4 Digits Code',
                        style: Theme.of(context).textTheme.headline4,
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 36),
                        child: Text(
                          'Enter the 4 digits code that you \nreceived on your email.',
                          style: Theme.of(context).textTheme.subtitle2!.copyWith(
                                fontSize: 14,
                                color: ThemeUtils.textColor.withOpacity(0.6),
                              ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      // Manual Login input fields
                      buildTextInputFields(),
                      SizedBox(
                        height: 16,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 8),
                        child: buildProceedButton(context),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 8),
                        child: Center(
                          child: Text(
                            'Enter $_defaultOTP to continue',
                            style: Theme.of(context).textTheme.subtitle1!.copyWith(
                                  fontSize: 12,
                                  color: Colors.redAccent,
                                ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Back button widget which is shown on top left of the screen
  Widget buildBackButton() {
    return IconButton(
      icon: Icon(Icons.chevron_left),
      onPressed: () => Navigator.of(context).maybePop(null),
    );
  }

  /// Make changes here regarding the Input Field required for this page, add or remove fields
  /// UI changes in the field are done in [FormInputField] class
  Widget buildTextInputFields() {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 40),
        child: buildOTPTextField(),
      ),
    );
  }

  /// Make changes here regarding the Input Field required for this page, add or remove fields
  /// Or update the length of code length to show the input UI properly
  Widget buildOTPTextField() {
    return PinPut(
      controller: _controller,
      fieldsCount: 4,
      autovalidateMode: AutovalidateMode.disabled,
      autofocus: true,
      keyboardType: TextInputType.number,
      textStyle: Theme.of(context).textTheme.subtitle2!.copyWith(
            fontSize: 20,
          ),
      selectedFieldDecoration: ThemeUtils.boxDecoration.copyWith(
        border: Border.all(color: ThemeUtils.accentColor),
      ),
      submittedFieldDecoration: ThemeUtils.boxDecoration,
      followingFieldDecoration: ThemeUtils.boxDecoration,
      disabledDecoration: ThemeUtils.boxDecoration.copyWith(
        color: Colors.transparent,
      ),
      inputDecoration: InputDecoration(
        fillColor: Colors.transparent,
        filled: true,
        isCollapsed: true,
        focusedBorder: ThemeUtils.buildOutlineInputBorder(
          width: 0.0,
          color: Colors.transparent,
        ),
        enabledBorder: ThemeUtils.buildOutlineInputBorder(
          width: 0.0,
          color: Colors.transparent,
        ),
        disabledBorder: ThemeUtils.buildOutlineInputBorder(
          width: 0.0,
          color: Colors.transparent,
        ),
        border: ThemeUtils.buildOutlineInputBorder(
          width: 0.0,
          color: Colors.transparent,
        ),
      ),
      validator: (value) {
        if ((value!.length) != 4) {
          _resetOTPField();
          showToast('Invalid OTP');
        }
        return null;
      },
      eachFieldWidth: 68,
      eachFieldHeight: 56,
      withCursor: false,
      focusNode: _focusNode,
      onSaved: (newValue) {
        _otp = newValue;
      },
      onChanged: (newValue) {
        _otp = newValue;
      },
    );

    // return PinPut(
    //   controller: _controller,
    //   fieldsCount: 4,
    //   autovalidateMode: AutovalidateMode.disabled,
    //   autofocus: true,
    //   keyboardType: TextInputType.number,
    //   textStyle: Theme.of(context).textTheme.subtitle2!.copyWith(
    //         fontSize: 20,
    //       ),
    //   selectedFieldDecoration: ThemeUtils.boxDecoration.copyWith(
    //     border: Border.all(color: ThemeUtils.accentColor),
    //   ),
    //   submittedFieldDecoration: ThemeUtils.boxDecoration,
    //   followingFieldDecoration: ThemeUtils.boxDecoration,
    //   disabledDecoration: ThemeUtils.boxDecoration.copyWith(
    //     color: Colors.transparent,
    //   ),
    //   inputDecoration: InputDecoration(
    //     fillColor: Colors.transparent,
    //     filled: true,
    //     isCollapsed: true,
    //     focusedBorder: ThemeUtils.buildOutlineInputBorder(
    //       width: 0.0,
    //       color: Colors.transparent,
    //     ),
    //     enabledBorder: ThemeUtils.buildOutlineInputBorder(
    //       width: 0.0,
    //       color: Colors.transparent,
    //     ),
    //     disabledBorder: ThemeUtils.buildOutlineInputBorder(
    //       width: 0.0,
    //       color: Colors.transparent,
    //     ),
    //     border: ThemeUtils.buildOutlineInputBorder(
    //       width: 0.0,
    //       color: Colors.transparent,
    //     ),
    //   ),
    //   validator: (value) {
    //     if ((value!.length ?? 0) != 4) {
    //       _resetOTPField();
    //       showToast('Invalid OTP');
    //     }
    //     return null;
    //   },
    //   eachFieldWidth: 68,
    //   eachFieldHeight: 56,
    //   withCursor: false,
    //   focusNode: _focusNode,
    //   onSaved: (newValue) {
    //     _otp = newValue;
    //   },
    //   onChanged: (newValue) {
    //     _otp = newValue;
    //   },
    // );
  }

  void _resetOTPField() {
    return runAfterBuild((_) {
      setState(() => _controller!.clear());
    });
  }

  /// Make changes here regarding the 'Login' button UI or function, if required
  Widget buildProceedButton(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Theme.of(context).accentColor,
      ),
      child: ListTile(
        title: Center(
          child: Text(
            'Continue',
            style: Theme.of(context).textTheme.subtitle2!.copyWith(color: Colors.white, fontSize: 16.0),
          ),
        ),
        trailing: Icon(
          Icons.arrow_forward_outlined,
          color: Colors.white,
        ),
        onTap: () {
          _proceed(context);
        },
      ),
    );
  }

  /// This method should handle all intermediate process before the actual
  /// login callback is called.
  ///
  /// For example, fetching and processing input data & validation to be done
  /// before calling login callback.
  void _proceed(BuildContext context) {
    final isAllValidated = _formKey.currentState!.validate();
    if (isAllValidated) {
      /// save all the values if validation passes
      _formKey.currentState!.save();

      if (_otp != _defaultOTP) {
        showToast('Invalid OTP');
        _resetOTPField();
        return;
      }
      final requestData = {
        KEY_OTP: _otp,
      };
      // call proceed method and process resulting data
      widget.verifyOTP!.call(widget.apiService!, requestData).then((status) {
        // Return the user instance to Called Widget
        Navigator.of(context).maybePop(status);
      });
    } else {}
  }

  String get _defaultOTP => '5822';
}
