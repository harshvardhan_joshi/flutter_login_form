import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import '../../login.dart';

/// This class represents the Login page in the app
///
/// This page provides following functions:
///  - Manual Login
///  - Login using Social Media
///  - Forgot password
///  - Navigation to Registration page. if user is not registered
///
/// Note: All of the above functionality are customizable to match major requirements
///
///
/// The social media login process is implemented in a way that if only proper
/// configuration is done for the project, it is ready to use without any code changes.
class LoginPage extends StatefulWidget {
  /// The callback used for performing tasks when user clicks the login button
  /// on Login page, as both Login and Registration pages are linked, we
  /// can provide this callbacks that are used in other page as well.
  ///
  /// It can be used to perform API call for login process.
  /// It takes a Map of request data, which should be sent as parameters in login
  /// request to API, this map of data resembles a simple json data structure.
  ///
  /// Also the callback must Return a [AppUser] instance created with the data
  /// that is received from the API.
  ///
  /// Following can be done within this callback:
  ///   - Any network call to API for login
  ///   - Validating the response and Creating [AppUser] instance as final result
  ///   - Catching or Throwing exception in case of failure in login process
  ///
  /// The [AppUser] Model can be updated as per the requirement. Also, creating a factory method
  /// to get [AppUser] instance from any other type of data is recommended.
  ///
  /// For reference,
  /// Check [User.fromFirebaseUser(firebaseUser)] Method in [AppUser] class.
  ///
  /// Note: This callback is supposed to be used for custom API calls only.
  final APICallback<void>? onLogin;

  /// The callback used for performing tasks when custom login process is executed
  /// by the [onLogin] or any social media icons.
  ///
  /// This callback can be used to handle failure scenarios.
  /// Following can be done within this callback:
  ///   - Identify and Handle the Error
  ///   - Re-throw the error if required to handle it from the caller method/widget
  ///
  ///
  /// This method will provide an instance of [Error], with some description and
  /// stack trace of the cause of exception.
  ///
  /// It can return any Custom Error as long as the custom error extends the [Error]
  /// class.
  ///
  /// For reference,
  /// Check [AuthError] or [APIError] class for Customized Errors
  final AuthErrorCallback? onLoginError;

  /// The callback used for performing tasks when user clicks any social media
  /// icons for Sign up process. i.e Login with google, facebook etc.
  ///
  /// It does not take custom parameters, but it provides a [AppUser]
  /// parameter to perform any additional tasks that are required after the login
  /// process is completed successfully
  ///
  /// The [AppUser] Model can be updated as per the requirement.
  /// Also, creating a factory method to get [AppUser] instance from any other
  /// type of data is recommended.
  ///
  /// For reference,
  /// Check [User.fromFirebaseUser(firebaseUser)] Method in [AppUser] class.
  ///
  /// Note: This callback is supposed to be used for Social media Login only.
  /// Also, the same callback is used for all requested social media
  final AuthCallback? onSocialMediaLoginSuccess;

  /// The callback used for performing tasks when user clicks the Register button
  /// on registration page, as both Login and Registration pages are linked, we
  /// can provide this callbacks that are used in other page as well.
  ///
  /// It can be used to perform API call for registering process.
  /// It takes a Map of request data, which should be sent as parameters in Sign Up
  /// request to API, this map of data resembles a simple json data structure.
  ///
  /// Also the callback must Return a [AppUser] instance created with the data
  /// that is received from the API.
  ///
  /// Following can be done within this callback:
  ///   - Any network call to API for Sign Up
  ///   - Validating the response and Creating [AppUser] instance as final result
  ///   - Catching or Throwing exception in case of failure in Sign Up process
  ///
  /// The [AppUser] Model can be updated as per the requirement. Also, creating a factory method
  /// to get [AppUser] instance from any other type of data is recommended.
  ///
  /// For reference,
  /// Check [User.fromFirebaseUser(firebaseUser)] Method in [AppUser] class.
  ///
  /// Note: This callback is supposed to be used for custom API calls only.
  final APICallback<bool>? onSignUp;

  /// The callback used for performing tasks when custom Sign up process is executed
  /// by the [onSignUp] or any social media icons.
  ///
  /// This callback can be used to handle failure scenarios.
  /// Following can be done within this callback:
  ///   - Identify and Handle the Error
  ///   - Re-throw the error if required to handle it from the caller method/widget
  ///
  ///
  /// This method will provide an instance of [Error], with some description and
  /// stack trace of the cause of exception.
  ///
  /// It can return any Custom Error as long as the custom error extends the [Error]
  /// class.
  ///
  /// For reference,
  /// Check [AuthError] or [APIError] class for Customized Errors
  final AuthErrorCallback? onSignUpError;

  /// this is the api service which will be used for manual login process
  final ApiService? apiService;

  const LoginPage({
    Key? key,
    this.onLogin,
    this.onLoginError,
    this.onSocialMediaLoginSuccess,
    this.onSignUp,
    this.onSignUpError,
    this.apiService,
  }) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a `GlobalKey<FormState>` which should be unique to each Page
  // with a form.
  final _formKey = GlobalKey<FormState>();

  String? email;
  String? password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: Theme.of(context).canvasColor,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Center(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      // Title of the screen
                      Text(
                        'Login',
                        style: Theme.of(context).textTheme.headline4,
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        'Please login to your account.',
                        style: Theme.of(context).textTheme.subtitle2!.copyWith(color: ThemeUtils.textColor.withOpacity(0.6)),
                      ),
                      // Social media login buttons
                      // TODO add or remove Social media login here to show or hide specific option
                      SocialMediaButtons(
                        allowedSignUps: [
                          SocialMedia.Google,
                          SocialMedia.FaceBook,
                          SocialMedia.LinkedIn,
                        ],
                        onSignInComplete: (user) async {
                          EasyLoading.dismiss();
                          _resetPageState();
                          if (user == null) return;
                          // TODO this call back will be called for all social media

                          /// Social media specific implementation is already done
                          /// Just use this call back to handle the final output
                          /// of the sign in process.
                          ///
                          /// The [user] instance provided by this method is of a Custom
                          /// model created for this demo called [AppUser]
                          ///
                          /// Feel free to update the [AppUser] model or replace it with any other
                          /// object if required.

                          EasyLoading.show();
                          widget.onSocialMediaLoginSuccess!.call(user);
                        },
                        onError: (error, [stackTrace]) async {
                          _resetPageState();
                          EasyLoading.dismiss();
                          // TODO handle social media login errors here
                          widget.onLoginError!.call(error, stackTrace);
                        },
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        'or Login with Email',
                        style: Theme.of(context).textTheme.subtitle2!.copyWith(color: ThemeUtils.textColor.withOpacity(0.6)),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      // Manual Login input fields
                      // TODO: customize input text fields in this method, you can add, change or remove fields
                      buildTextInputFields(),
                      SizedBox(
                        height: 4,
                      ),
                      // TODO: customize handling of forget password in this method
                      buildForgotPassword(context),
                      SizedBox(
                        height: 12,
                      ),
                      // Login Button
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 8),
                        child: buildLoginButton(context),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24),
                        child: buildRegisterNow(context),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Make changes here regarding the 'Register Now' UI or function, if required
  Widget buildRegisterNow(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('Don’t have an account?'),
        SizedBox(width: 8),
        InkWell(
          onTap: () {
            _resetPageState();
            showRegistrationPage(
              context: context,
              apiService: widget.apiService,
              onLogin: widget.onLogin,
              onLoginError: widget.onLoginError,
              onSignUp: widget.onSignUp,
              onSignUpError: widget.onSignUpError,
              onSocialMediaLoginSuccess: widget.onSocialMediaLoginSuccess,
            );
          },
          child: Text(
            'Register now',
            style: Theme.of(context).textTheme.subtitle1!.copyWith(
                  color: Theme.of(context).accentColor,
                  fontWeight: FontWeight.bold,
                ),
          ),
        ),
      ],
    );
  }

  /// Make changes here regarding the 'Forgot Password' button UI or function, if required
  Widget buildForgotPassword(BuildContext context) {
    return InkWell(
      onTap: () async {
        _resetPageState();
        final response = await showForgotPasswordPage(
          context: context,
          apiService: widget.apiService,
        );
        if (isEmpty(response)) return;
        showToast(response![KEY_MESSAGE]);
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 40),
        child: Align(
          alignment: Alignment.centerRight,
          child: Text(
            'Forgot Password?',
            style: Theme.of(context).textTheme.subtitle2!.copyWith(
                  fontSize: 14,
                  color: ThemeUtils.textColor.withOpacity(0.6),
                ),
          ),
        ),
      ),
    );
  }

  /// Make changes here regarding the Input Field required for this page, add or remove fields
  /// UI changes in the field are done in [FormInputField] class
  Widget buildTextInputFields() {
    // TODO: add remove input fields here for login form
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FormInputField(
              title: 'Email Address',
              inputType: TextInputType.emailAddress,
              inputAction: TextInputAction.next,
              validator: emailValidator,
              onSaved: (value) => email = value,
            ),
            FormInputField(
              title: 'Password',
              inputType: TextInputType.text,
              inputAction: TextInputAction.go,
              validator: passwordValidator,
              obscureText: true,
              onSaved: (value) => password = value,
            ),
          ],
        ),
      ),
    );
  }

  /// Make changes here regarding the 'Login' button UI or function, if required
  Widget buildLoginButton(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Theme.of(context).accentColor,
      ),
      child: ListTile(
        title: Center(
          child: Text(
            'Login',
            style: Theme.of(context).textTheme.subtitle2!.copyWith(color: Colors.white, fontSize: 16.0),
          ),
        ),
        trailing: Icon(
          Icons.arrow_forward_outlined,
          color: Colors.white,
        ),
        onTap: () {
          _processLogin(context);
        },
      ),
    );
  }

  /// This method should handle all intermediate process before the actual
  /// login callback is called.
  ///
  /// For example, fetching and processing input data & validation to be done
  /// before calling login callback.
  void _processLogin(BuildContext context) {
    final isAllValidated = _formKey.currentState!.validate();
    if (isAllValidated) {
      /// save all the values if validation passes
      _formKey.currentState!.save();

      // TODO: replace data here to customize login parameters sent to api
      final requestData = <String, dynamic>{
        KEY_EMAIL: email,
        KEY_PASSWORD: password,
      };

      final apiService = widget.apiService;

      assert(apiService != null, 'Api Service is required for manual login to work');
      EasyLoading.show();
      // call login method and process resulting data
      widget.onLogin!.call(apiService!, requestData).then((user) {
        return user;
      }).catchError((e, s) {
        widget.onLoginError!.call(e, s);
      });
    }
  }

  /// This method is used to dismiss all the validation messages from the
  /// text form fields in the form associated with [_formKey]
  void _resetPageState() {
    setState(() {
      _formKey.currentState!.reset();
    });
  }
}
