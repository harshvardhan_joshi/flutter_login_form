import 'package:flutter/material.dart';

import '../../login.dart';

/// Forgot password screen
class ForgotPassword extends StatefulWidget {
  /// An API callback to execute when user presses the continue button
  ///
  /// This call should:
  ///  - contact the API responsible for Forget password module
  ///  - get a response from the API
  ///  - process and return the response to caller widget
  final APICallback<Map<String, dynamic>>? onProceed;

  /// api used for processing the forget password
  final ApiService? apiService;

  const ForgotPassword({
    Key? key,
    @required this.onProceed,
    this.apiService,
  }) : super(key: key);

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a `GlobalKey<FormState>` which should be unique to each Page
  // with a form.
  final _formKey = GlobalKey<FormState>();

  /// email value which is provided by the user in the [FormInputField]
  /// in this page.
  String? email;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(
          color: ThemeUtils.textColor,
        ),
        leading: buildBackButton(),
      ),
      body: SafeArea(
        child: Container(
          color: Theme.of(context).canvasColor,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Center(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      // Title of the screen
                      Text(
                        'Forgot Password',
                        style: Theme.of(context).textTheme.headline4,
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 36),
                        child: Text(
                          'Enter your email for the verification process,\nWe will send 4 digit code to your email.',
                          style: Theme.of(context).textTheme.subtitle2!.copyWith(
                                fontSize: 14,
                                color: ThemeUtils.textColor.withOpacity(0.6),
                              ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      // Manual Login input fields
                      buildTextInputFields(),
                      SizedBox(
                        height: 16,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 8),
                        child: buildProceedButton(context),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// The back button widget which is shown on top left of the screen
  Widget buildBackButton() {
    return IconButton(
      icon: Icon(Icons.chevron_left),
      onPressed: () => Navigator.of(context).maybePop(null),
    );
  }

  /// Make changes here regarding the Input Field required for this page, add or remove fields
  /// UI changes in the field are done in [FormInputField] class
  Widget buildTextInputFields() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FormInputField(
              title: 'Email Address',
              inputType: TextInputType.emailAddress,
              inputAction: TextInputAction.next,
              validator: emailValidator,
              onSaved: (value) => email = value,
            ),
          ],
        ),
      ),
    );
  }

  /// Make changes here regarding the 'Login' button UI or function, if required
  Widget buildProceedButton(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Theme.of(context).accentColor,
      ),
      child: ListTile(
        title: Center(
          child: Text(
            'Continue',
            style: Theme.of(context).textTheme.subtitle2!.copyWith(color: Colors.white, fontSize: 16.0),
          ),
        ),
        trailing: Icon(
          Icons.arrow_forward_outlined,
          color: Colors.white,
        ),
        onTap: () {
          // TODO implement the handling of the forget password process
          final response = _proceed(context);
        },
      ),
    );
  }

  /// This method should handle all intermediate process before the actual
  /// login callback is called.
  ///
  /// For example, fetching and processing input data & validation to be done
  /// before calling login callback.
  Future _proceed(BuildContext context) async {
    final isAllValidated = _formKey.currentState!.validate();
    if (isAllValidated) {
      /// save all the values if validation passes
      _formKey.currentState!.save();

      // Gather Request data for sending a login request
      final requestData = <String, dynamic>{
        KEY_EMAIL: email,
      };
      // call login method and process resulting data
      var apiService = widget.apiService;
      try {
        final response = await widget.onProceed!.call(apiService!, requestData);
        final isSuccess = response[KEY_SUCCESS];
        if (isSuccess) {
          return _proceedWithOTP(context);
        }
      } catch (error) {
        var forgotPasswordError = getForgotPasswordError(error);
        showToast(getFormattedErrorMessage(forgotPasswordError));
        return null;
      }
    }
    return null;
  }

  /// Method to handle verification of OTP
  Future<dynamic> _proceedWithOTP(BuildContext context) {
    // 1. Get Verification status from OTP page
    return showOTPPage(context)!.then((status) async {
      status ??= false;

      // 2. Verify the OTP status
      if (!status) {
        //if not verified => return null as new password
        Navigator.of(context).maybePop(null);
        return null;
      }

      // 3. OTP verified, continue with reset password process
      // 3.1 get new password from reset password page
      return showResetPassword(context)!.then(
        (jsonData) {
          // 3.2 get status/message from the api and return the final status
          return Navigator.of(context).maybePop(jsonData);
        },
      );
    });
  }
}
