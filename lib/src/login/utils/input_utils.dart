import 'package:flutter/material.dart';
import 'package:flutter_login_form/src/login/login.dart';

// TODO: Add any input text field related methods or helper properties here

// TODO update this validator as per the requirement, current one will work in most cases
final FormFieldValidator<String> fullNameValidator = (String? value) {
  if (isEmpty(value)) {
    return 'Please enter full name';
  }
  var nameLength = (value!.length);

  if (nameLength < 3) {
    return "name must be at least 3 characters long";
  }

  final isValidName = fullNameRegEx.hasMatch(value);
  if (!isValidName) {
    return "Please enter a valid full name";
  }
  return null;
};

// TODO update this validator as per the requirement, current one will work in most cases
final FormFieldValidator<String> emailValidator = (String? value) {
  if (isEmpty(value)) {
    return 'Please enter email';
  }
  final isValidEmail = emailRegEx.hasMatch(value!);
  if (!isValidEmail) {
    return "Please enter valid email";
  }
  return null;
};

RegExp get emailRegEx {
  return RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
}

RegExp get fullNameRegEx {
  return RegExp(r'^[a-zA-Z ]*$');
}

// TODO update this validator as per the requirement
final FormFieldValidator<String> passwordValidator = (String? value) {
  if (isEmpty(value)) {
    return 'Please enter password';
  }

  var passwordLength = (value!.length);

  if (passwordLength <= 5) {
    return "password length must be at least 6 characters";
  }
  return null;
};

// TODO update this validator as per the requirement, current one will work in most cases
final String Function(String value1, String value2) confirmPasswordValidator = (String value1, String value2) {
  if (isEmpty(value2)) {
    return 'Please enter confirm password';
  }

  final validation3 = value1 != null && value1 == value2;
  if (!validation3) {
    return "Confirm password must match with New Password";
  }
  return '';
};
