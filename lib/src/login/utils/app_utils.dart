import 'dart:ui';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:simple_linkedin_login/simple_linkedin_login.dart';

import '../login.dart';

// TODO: Add any app related methods or helper properties here

runAfterBuild(FrameCallback frameCallback) {
  WidgetsBinding.instance!.addPostFrameCallback(frameCallback);
}

/// Helper method to show any flutter widget page
///
/// [showAsReplacement] flag is used to decide the whether the new page will be
/// added on top of the current page or will replace the current page
Future<T?> showPage<T>(
  BuildContext? context,
  MaterialPageRoute<T> pageRoute, {
  bool showAsReplacement = false,
}) async {
  final navigator = Navigator.of(context!);
  if (showAsReplacement) {
    return navigator.pushReplacement(pageRoute);
  } else {
    return navigator.push(pageRoute);
  }
}

/// This method is to be used to only transition from Registration page
/// to Login page.
Future<AppUser?>? showLoginPage(
    {BuildContext? context,
    ApiService? apiService,
    onSignUp,
    onSignUpError,
    onLogin,
    onLoginError,
    onSocialMediaLoginSuccess,
    showAsReplacement = false}) {
  var pageRoute = MaterialPageRoute<AppUser>(
    builder: (context) => LoginPage(
      apiService: apiService!,
      onSignUp: onSignUp,
      onSignUpError: onSignUpError,
      onLogin: onLogin,
      onLoginError: onLoginError,
      onSocialMediaLoginSuccess: onSocialMediaLoginSuccess,
    ),
  );

  // Calling a utility method to start the transition
  return showPage<AppUser>(
    context,
    pageRoute,
    showAsReplacement: showAsReplacement,
  );
}

/// This method is to be used to only transition from login page to
/// Registration page.
Future<AppUser?>? showRegistrationPage(
    {BuildContext? context,
    ApiService? apiService,
    onSignUp,
    onSignUpError,
    onLogin,
    onLoginError,
    onSocialMediaLoginSuccess,
    showAsReplacement = false}) {
  var pageRoute = MaterialPageRoute<AppUser>(
    builder: (context) => SignUpPage(
      apiService: apiService!,
      onSignUp: onSignUp,
      onSignUpError: onSignUpError,
      onLogin: onLogin,
      onLoginError: onLoginError,
      onSocialMediaLoginSuccess: onSocialMediaLoginSuccess,
    ),
  );

  // Calling a utility method to start the transition
  return showPage<AppUser>(
    context!,
    pageRoute,
    showAsReplacement: showAsReplacement,
  );
}

/// This method is to be used to only transition from login page to
/// Registration page.
Future<Map<String, dynamic>?>? showForgotPasswordPage({
  BuildContext? context,
  ApiService? apiService,
}) {
  var pageRoute = MaterialPageRoute<Map<String, dynamic>>(
    builder: (context) => ForgotPassword(
      apiService: apiService!,
      onProceed: (apiService, requestData) async {
        return apiService.forgotPassword(requestData);
      },
    ),
  );

  // Calling a utility method to start the transition
  return showPage<Map<String, dynamic>>(context!, pageRoute);
}

/// This method is to be used to only transition from login page to
/// Registration page.
Future<bool?>? showOTPPage(BuildContext context) {
  var pageRoute = MaterialPageRoute<bool>(
    builder: (context) => CodeVerificationPage(
      verifyOTP: (apiService, requestData) async {
        final otp = requestData[KEY_OTP];

        // TODO: use OTP value here with the apiService to verify it and then return status
        final isVerified = true; // dummy status
        showToast('Verified, Please enter new Password');
        return isVerified;
      },
    ),
  );

  // Calling a utility method to start the transition
  return showPage<bool>(context, pageRoute);
}

/// This method is to be used to only transition from login page to
/// Registration page.
Future<Map<String, dynamic>?>? showResetPassword(BuildContext context) {
  var pageRoute = MaterialPageRoute<Map<String, dynamic>>(
    builder: (context) => ResetPassword(
      resetPassword: (apiService, result) async {
        return result;
      },
    ),
  );

  // Calling a utility method to start the transition
  return showPage<Map<String, dynamic>>(context, pageRoute);
}

void showToast(
  String? msg, {
  Color? backgroundColor,
  Color? textColor,
  bool isShort = true,
}) {
  Fluttertoast.showToast(
    msg: msg!,
    backgroundColor: backgroundColor,
    textColor: textColor,
    toastLength: isShort ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG,
    gravity: ToastGravity.SNACKBAR,
  );
}

String get defaultErrorMessage => 'Social Media sign-up failed';

String getFormattedErrorMessage(e) {
  if (e != null) {
    String message = e.toString();
    if (e is CustomError ||
        // e is FacebookAuthException ||
        e is FirebaseException) {
      message = e.message;
    } else if (e is LinkedInAuthError) {
      message = e.description;
    }
    return message;
  }
  return defaultErrorMessage;
}
