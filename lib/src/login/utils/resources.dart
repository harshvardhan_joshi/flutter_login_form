// TODO: Add any resources or properties here that can be used from anywhere in the app

const defaultTimeoutDuration = Duration(seconds: 30);

const String newUserProfileImageUrl = 'https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png';

const String splashLogoAsset = 'assets/logos/flutter_logo.png';
const String googleImageAsset = 'assets/logos/google_light.svg';
const String facebookImageAsset = 'assets/logos/facebook_new.svg';
const String twitterImageAsset = 'assets/logos/twitter.svg';
const String linkedInImageAsset = 'assets/logos/linkedin_new.svg';

//////////// Linked In Credentials //////////
const String linkedInClientID = '77bp3a0fmq9xvn';
const String linkedInClientSecret = 'KS0H9iZqP2A90qA8';
const String linkedInRedirectUrl = 'https://www.rockersinfo.com';
String? accessTokenLinkedIn;
//////////// Linked In Credentials //////////

//////////// Internal keys //////////
const KEY_EMAIL = 'email';
const KEY_OTP = 'otp';
const KEY_PASSWORD = 'password';
const KEY_VERIFICATION_STATUS = 'verification';
const KEY_FULL_NAME = 'fullName';
const KEY_SUCCESS = 'success';
const KEY_MESSAGE = 'message';
//////////// Internal keys //////////

//////////// Custom API Credentials //////////
const String apiBaseUrl = 'https://rockerstech.com/donation-reward-merge/api';
//////////// Custom API Credentials //////////

//////////// Network keys //////////

String? userToken;

const String guest_suffix = '/login/guest';

class HeaderKeys {
  static const token = 'www-token';
  static const initialToken = 'fscrwsf-token';
}

// Guest
class GuestKeys {
  static const initialTokenValue = 'w+QYVDLmJM0kZcq36cvjlA==';

  static const responseSuccess = 'success';
  static const responseData = 'data';
  static const responseToken = 'token';
  static const responseMessage = 'message';
  static const responseErrorCode = 'error_code';
}

// Login
class LoginKeys {
  static const requestEmail = 'email';
  static const requestPassword = 'password';

  static const responseSuccess = 'success';
  static const responseData = 'data';
  static const responseUserID = 'user_id';
  static const responseFirstName = 'first_name';
  static const responseLastName = 'last_name';
  static const responseEmail = 'email';
  static const responseUserImageUrl = 'user_image_url';
  static const responseMessage = 'message';
  static const responseErrorCode = 'error_code';
}

// Login
class RegistrationKeys {
  static const requestEmail = 'email';
  static const requestPassword = 'password';
  static const requestFullName = 'full_name';

  static const responseSuccess = 'success';
  static const responseData = 'data';
  static const responseUserID = 'user_id';
  static const responseFirstName = 'first_name';
  static const responseLastName = 'last_name';
  static const responseEmail = 'email';
  static const responseUserImageUrl = 'user_image_url';
  static const responseMessage = 'message';
  static const responseErrorCode = 'error_code';
}

//////////// Internal keys //////////
