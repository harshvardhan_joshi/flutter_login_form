/// Method is an extension to check if the provided
/// [data] is null or empty
bool isEmpty(dynamic data) {
  if (data is Iterable || data is Map || data is String) {
    return data.isEmpty ?? true;
  } else {
    return data == null;
  }
}

/// Method is an extension to check if the provided
/// [data] is not null and empty
bool isNotEmpty(dynamic data) {
  return !isEmpty(data);
}

/// Method to add only those values from [newList] which are not already added in the
/// [sourceList]
List<T> addNonRepeatingValues<T>(List<T> sourceList, List<T> newList) {
  for (final value in newList) {
    if (!sourceList.contains(value)) {
      sourceList.add(value);
    }
  }
  return sourceList;
}

/// Method to throw an exception if the given [data] contains HTML text
void throwErrorIfContainsHTML(String data) {
  final hasHtml = containsHtml(data);
  if (hasHtml) {
    throw ArgumentError('''
        
        HTML tags are not expected in response. response.body: $data
        
        ''');
  }
}

/// Method to check if the given string contains any HTML type text.
///
/// This method is still not 100% full proof because the XML format also fits in
/// this reg ex
bool containsHtml(String body) {
  final findHtmlTags = RegExp(r'<[^>]*>');
  final containsHtml = findHtmlTags.hasMatch(body);
  return containsHtml;
}

/// this method allows trimming the stack trace to relevant stack
/// for current application which is 10-20 call traces
///
/// defaults:
///   [stackTrace] : StackTrace.current
///   [count] : 15
///   [skip] : 1
String getTrimmedStackTrace({StackTrace? stackTrace, int count = 15, int skip = 1}) {
  stackTrace ??= StackTrace.current;
  return stackTrace.toString().split('\n').take(count + skip).skip(skip).join('\n');
}

/// Getter for easy access
StackTrace get trimmedStackTrace => StackTrace.fromString(getTrimmedStackTrace(skip: 2));
