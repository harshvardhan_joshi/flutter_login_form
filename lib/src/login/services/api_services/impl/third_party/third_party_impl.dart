import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

// import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:flutter_login_form/src/login/login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:simple_linkedin_login/simple_linkedin_login.dart';
import 'package:twitter_login/entity/auth_result.dart';
import 'package:twitter_login/twitter_login.dart';

// TODO: This file includes general implementation of Google, Facebook & linked in login.

/// It is Ready to use after required configurations are done for each.
///
/// Google: Google login configuration with Firebase authentication
/// Facebook: Facebook login configuration with Facebook developer api & Firebase authentication
/// Linkedin: Linkedin login configuration with LinkedIn developer api

/// Implementation of the Api Service for Google sign-in
///
/// Note: If firebase configuration is done correctly, then google sign-in
/// should work without any code changes.
class GoogleApiServiceImpl extends ApiService<AppUser> {
  GoogleApiServiceImpl(onApiError) : super(onApiError);

  final GoogleSignIn googleSignIn = GoogleSignIn();

  @override
  Future<AppUser> signIn(Map<String, dynamic> requestData) async {
    final userCred = await signInWithGoogle().catchError((e) => throw e);

    if (userCred == null) return Future<AppUser>.value(null);
    return AppUser.fromFirebaseUser(userCred.user!);
  }

  Future<UserCredential> signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await googleSignIn.signIn().catchError((e) => throw getSocialMediaSignUpError(e));

    if (googleUser == null) {
      throw CancelledByUserError('Google login is cancelled by user');
    }

    // Obtain the auth details from the request
    final GoogleSignInAuthentication googleAuth = await googleUser.authentication;

    // Create a new credential
    final OAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    if (credential == null) {
      throw SocialSignUpError('Access to Google profile data is denied');
    }
    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }

  @override
  Future<void> signOut() async {
    await googleSignIn.disconnect();
    return null;
  }
}

// /// Implementation of the Api Service for Facebook sign-in
// ///
// /// Note: If firebase and facebook configuration is done correctly,
// /// then facebook sign-in should work without any code changes.
// class FacebookApiServiceImpl extends ApiService<AppUser> {
//   FacebookApiServiceImpl(onApiError) : super(onApiError);
//
//   final facebookAuth = FacebookAuth.instance;
//
//   static const String FB_NAME = 'name';
//   static const String FB_EMAIL = 'email';
//   static const String FB_PICTURE = 'picture';
//   static const String FB_PICTURE_DATA = 'data';
//   static const String FB_PICTURE_URL = 'url';
//
//   @override
//   Future<AppUser> signIn(Map<String, dynamic> requestData) async {
//     final userCred = await signInWithFacebook().catchError((e) {
//       throw e;
//     });
//
//     if (isEmpty(userCred)) return null;
//
//     final userData = await facebookAuth.getUserData();
//
//     // Fetch User Data
//     final name = userData[FB_NAME];
//     final email = userData[FB_EMAIL];
//     final pictureUrl = userData[FB_PICTURE][FB_PICTURE_DATA][FB_PICTURE_URL];
//
//     // Create user from the user's data
//     return AppUser(name: name, email: email, profilePictureUrl: pictureUrl);
//   }
//
//   Future<UserCredential> signInWithFacebook() async {
//     // Trigger the sign-in flow
//
//     final AccessToken accessToken = await facebookAuth.login(
//       loginBehavior: LoginBehavior.NATIVE_WITH_FALLBACK,
//     );
//
//     if (accessToken == null) {
//       throw getSocialMediaSignUpError(null,
//           message: "Couldn't obtain access token from facebook, try again.");
//     }
//     // Create a credential from the access token
//     final FacebookAuthCredential facebookAuthCredential =
//         FacebookAuthProvider.credential(accessToken.token);
//
//     if (facebookAuthCredential == null) {
//       throw CancelledByUserError("Facebook login is cancelled by user");
//     }
//     // Once signed in, return the UserCredential
//     var facebookCred = await FirebaseAuth.instance
//         .signInWithCredential(facebookAuthCredential)
//         .catchError((e) => throw getSocialMediaSignUpError(e));
//     if (facebookCred == null) {
//       throw SocialSignUpError('Access to Facebook profile data is denied');
//     }
//     return facebookCred;
//   }
//
//   @override
//   Future<void> signOut() {
//     return facebookAuth.logOut();
//   }
// }

/// ----- Twitter login ----- ///

const String TWITTER_REDIRECT_URL = 'flutterloginexample://';

String twitterApiKey = '8nCU1iQ73K7FQ99Z5uBa8wHwc';
String twitterApiKeySecret = 'lNjwZfxJmtsxZzDGbmsrI16wHPt3w3ElandjmTAHz6iURHayIy';

class TwitterApiServiceImpl extends ApiService<AppUser> {
  TwitterApiServiceImpl(onApiError) : super(onApiError);

  final twitterLogin = TwitterLogin(
    apiKey: twitterApiKey,
    apiSecretKey: twitterApiKeySecret,
    redirectURI: TWITTER_REDIRECT_URL,
  );

  @override
  Future<AppUser> signIn(Map<String, dynamic> parameters) async {
    final result = await signInWithTwitter().catchError((e) => throw e);

    if (isEmpty(result)) return Future<AppUser>.value(null);

    AppUser userData;
    switch (result.status) {
      case TwitterLoginStatus.loggedIn:
        userData = AppUser(
          name: result.user!.name,
          profilePictureUrl: result.user!.thumbnailImage,
          email: result.user!.email,
        );
        break;
      case TwitterLoginStatus.cancelledByUser:
        var twitterError = 'Twitter login cancelled by the user';
        showToast(twitterError);
        throw CancelledByUserError(twitterError);
        break;
      case TwitterLoginStatus.error:
        var twitterError = result.errorMessage;
        showToast(twitterError);
        throw getSocialMediaSignUpError(null, message: twitterError);
        break;
    }

    return Future<AppUser>.value(null);
  }

  Future<AuthResult> signInWithTwitter() async {
    // Trigger the sign-in flow
    final twitterResponse = await twitterLogin.login(forceLogin: true);

    if (twitterResponse == null) {
      throw getSocialMediaSignUpError(null, message: "Couldn't obtain data access from twitter, try again.");
    }
    return twitterResponse;
  }
}

/// Implementation of the Api Service for LinkedIn sign-in
///
/// Note: If LinkedIn app configuration is done correctly, then LinkedIn sign-in
/// should work without any code changes.
class LinkedInApiServiceImpl extends ApiService<AppUser> {
  final VoidCallback? showLoading;
  final VoidCallback? stopLoading;

  LinkedInApiServiceImpl(
    onApiError, {
    this.showLoading,
    this.stopLoading,
  }) : super(onApiError);

  static void initializeLinkedInLogin(BuildContext context) {
    LinkedInLoginClient.initialize(context, clientId: linkedInClientID, clientSecret: linkedInClientSecret, redirectUri: linkedInRedirectUrl);
  }

  Future<String> _getAccessToken() {
    return LinkedInLoginClient.loginForAccessToken().then((token) => accessTokenLinkedIn = token).catchError((e) {
      if (e is LinkedInAuthError && e.description.toLowerCase() == 'Unknown Error'.toLowerCase()) {
        throw CancelledByUserError('Linked in login is cancelled by user');
      }
      throw getSocialMediaSignUpError(e);
    });
  }

  @override
  Future<AppUser> signIn(Map<String, dynamic> requestData) async {
    stopLoading!.call();
    String accessToken = await _getAccessToken().catchError((e) {
      throw e;
    });

    if (isEmpty(accessToken)) {
      throw CancelledByUserError('Linked in login is cancelled by user');
    }

    showLoading!.call();
    final emailDoc = await LinkedInLoginClient.getEmail();
    final profileDoc = await LinkedInLoginClient.getProfile();

    if (emailDoc == null || profileDoc == null) {
      throw SocialSignUpError('Access to Linked in profile data is denied');
    }

    final fullName = '${profileDoc.fullName}';
    final email = emailDoc.primaryEmail ?? '--';
    final profilePictureUrl = profileDoc.profilePictureUrl;

    print('email: $email');
    print('fullName: $fullName');
    print('profilePictureUrl: $profilePictureUrl');
    stopLoading!.call();
    return AppUser(
      name: fullName,
      profilePictureUrl: profilePictureUrl,
      email: email,
    );
  }

  @override
  Future<void> signOut() {
    return Future<void>.value(null);
  }
}
