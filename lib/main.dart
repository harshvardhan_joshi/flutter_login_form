import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dynamic_themes/dynamic_themes.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_login_form/src/src.dart';

import 'src/app/view/connectivity_manager.dart';
import 'src/login/login.dart';

Future<void> main() async {
  await _setupServices();
  runApp(RockersApp());
}

Future<void> _setupServices() async {
  WidgetsFlutterBinding.ensureInitialized();
  await _initializeFirebase();

  /// --- Connectivity check ----

  if (connectivityManager.isOffline) {
    return;
  }
}

Future<void> _initializeFirebase() async {
  await Firebase.initializeApp();
  await _initializeCrashlytics();
}

Future<void> _initializeCrashlytics() async {
  if (kDebugMode) {
    // Force disable Crashlytics collection while doing every day development.
    // Temporarily toggle this to true if you want to test crash reporting in your app.
    await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
  } else {
    // Handle Crashlytics enabled status when not in Debug,
    // e.g. allow your users to opt-in to crash reporting.
    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
  }
}

/// This app is demo for using the Login and Registration modules
class RockersApp extends StatefulWidget {
  @override
  _RockersAppState createState() => _RockersAppState();
}

class _RockersAppState extends State<RockersApp> {
  @override
  Widget build(BuildContext context) {
    return DynamicTheme(
      themeCollection: ThemeUtils.collection,
      defaultThemeId: AppThemes.Light,
      builder: (_, theme) {
        return /*ConnectivityChangeBuilder(
          connectedBuilder: (context) {
            return*/
            MaterialApp(
          title: 'QuickStart Flutter App',
          theme: theme,
          builder: EasyLoading.init(),
          home: buildApp(),
          debugShowCheckedModeBanner: false,
        );
        /*},
          connectivityLoadingBuilder: (context) {
            return LoadingConnectivityWidget();
          },
        );*/
      },
    );
  }

  /// Show app with a loading screen
  FutureBuilder<bool> buildApp() {
    return FutureBuilder<bool>(
        key: ValueKey('main_timer'),
        future: Future<bool>.delayed(
          Duration(seconds: 5),
          () => Future.value(true),
        ),
        builder: (context, snapshot) {
          final showHome = snapshot.data ?? false;
          if (showHome) {
            return buildHomePage();
          }
          return buildSplashScreen();
        });
  }

  Widget buildHomePage() {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
      ),
      body: Center(
        child: BodyWidget(),
      ),
    );
  }

  Widget buildSplashScreen() {
    return SplashScreen();
  }

  bool _isOffline(ConnectivityResult connectionState) {
    return connectionState == ConnectivityResult.none;
  }
}
